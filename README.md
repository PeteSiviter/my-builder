# MyBuilder PHP Code Exercise

Thank you for agreeing to complete the MyBuilder coding exercise. There is no "right" answer to this instead we are just looking to see how you approach the problem and what choices you make. 

Please send your completed test back within 72 hours whatever state it is in. Sendingus the git repo you created for it is ideal but a zip of the code is acceptable.

If you have any questions or need clarification then please email us back. 

We do ask that you do not upload this test or your answer to a public repository. 

## Part 1: Review of crap code 
Reviewing the code of other engineers at MyBuilder is an important part of working here. 

We have deliberately written a lot of crap PHP code in the crapCode.php file in the root directory.

Please do a code review of that code and tell us all the problems you can find in and explain for each
one why it is a problem. Do not write a new solution, although additional suggestions to as a better way of doing it is good.

## Part 2: NPS Test

### Introduction
We have decided to monitor the happiness of our tradespeople by asking 5% of them each week by the Net promoter score (NPS) question which is as follows: 

> “How likely is it that you would recommend MyBuilder to a friend or colleague?“

The users answer on a scale from **0** *(Not likely)* to **10** *(Very likely)*.

These scores are provided in the form of CSV files every week. We need to validate and import the data, and store it in our database.
Each user can only give their score only once per week.

### NPS calculation
Based on these scores they are ranked as Promoters, Detractors and Passives.

* Promoters (score 9-10) are loyal enthusiasts who will keep buying and refer others, fueling growth.
* Passives (score 7-8) are satisfied but unenthusiastic customers who are vulnerable to competitive offerings.
* Detractors (score 0-6) are unhappy customers who can damage your brand and impede growth through negative word-of-mouth.

The NP score is calculated by taking the percentage of customers who are Promoters and subtracting the percentage who are Detractors.

### Task
We would like you to finish the import command and create other code to achieve the required outcomes below.
How you do that is completely up to you but we encourage you to write tests. 

You can change everything we have given you, except for the data files and this readme.

Good luck, and we are looking forward to seeing your solution! 

### Required outcome
* Import the data from the weekly NPS files
* Output the NP score for a given week (and optional year)  

### Notes
You run Symfony commands through a console application, e.g.:

```bash
cd src/
php console mybuilder:code-test:import-nps-scores
```

#### Database
The database will be a SQLite3 file in: `src/Persistence/nps.db`. To create it simply run:

```bash
src/Persistence/create_db.sh
```

There is one table called `np_score`.

Feel free to change the schema and/or add other tables. If you do, simply overwrite the DB dump and commit your changes:

```bash
src/Persistence/export_db.sh
```

#### CSV files
The CSV files are in `src/data`.

### Links
* Net Promoter Score (NPS):
  * https://en.wikipedia.org/wiki/Net_Promoter
  * https://www.netpromoter.com/
* Symfony Console:
  * https://symfony.com/doc/current/console.html
* SQLite3:
  * https://sqlite.org/docs.html
  * http://php.net/manual/en/book.sqlite3.php

