<?php

namespace MyBuilder\CodeTestNPS\Command;

use DirectoryIterator;
use Exception;
use PDO;
use SplFileInfo;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Dotenv\Dotenv;

class ImportNPSScoreCommand extends Command
{
    private ?string $path;

    static function checkId($a, $b)
    {
        return (int)$a[0] - (int)$b[0];
    }

    protected function configure()
    {
        $this
            ->setName('mybuilder:code-test:import-nps-scores')
            ->setDescription('This command imports NPS scores from a given CSV file')
            ->addArgument('dbpath', InputArgument::OPTIONAL, 'Path to the database')
            ->addArgument('csvdir', InputArgument::OPTIONAL, 'Path to the csv files to import');

    }



    /**
     * @throws Exception
     */

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // The entire dotEnv in here is just an example, it's not needed as per the spec, but provides
        // for a "config file" option or a cli option to import the csv's
        $dotEnv = new Dotenv();
        $envPath = $this->getProjectDir() . DIRECTORY_SEPARATOR . ".env";
        $dotEnv->load($envPath);

        if (empty($dbFile = $input->getArgument('dbpath'))) {
            $dbFile = $_ENV['DB_FILE_PATH'];
        }
        if (empty($csvDir = $input->getArgument('csvdir'))) {
            $csvDir = $_ENV['CSV_DIR'];
        }

        if (!empty($dbFile) && !empty($csvDir)) {
            if (file_exists($dbFile) && is_writable($dbFile)) {
                $output->writeln("The file $dbFile exists. It is going to be deleted.");
                unlink($dbFile);
                // TODO: better error handling here
            }
        } else {
            throw(new Exception("Incomplete parameters provided"));
        }

        $output->writeln("Attempting to import CSV files from: " . $csvDir . " into DB: " . $dbFile);
        $db = new PDO("sqlite:$dbFile");

        $sql = 'CREATE TABLE np_score(user_id INTEGER, score INTEGER, week_number INTEGER, year INTEGER)';
        $statement = $db->prepare($sql);
        $statement->execute();

        $dir = new DirectoryIterator(dirname($csvDir));
        /** @var SplFileInfo $fileInfo */
        foreach ($dir as $fileInfo) {
            if (!$fileInfo->isDot()) {
                $this->importCsv($output, dirname($csvDir) . DIRECTORY_SEPARATOR . $fileInfo->getFilename(), $db);
            }
        }
    }

    // Remember these are tricky to mock...

    /**
     * @TODO: There is probably a way of getting the project root in a command class through Symfony here, would
     * like to spend some time Googling but the test needs to be done...
     * @return string
     */
    public function getProjectDir(): string
    {
        return (realpath(__DIR__ . '../../..'));
    }

    /**
     * Import a single CSV file
     * @param $csvFile
     * @param $db
     * @param $week
     * @param $year
     * @return void
     * @throws Exception
     */
    protected function importCsv($output, $csvFile, $db)
    {
        $output->writeln("Attempting to import CSV: " . $csvFile);

        $fileName = basename($csvFile, ".csv");
        $fileParts = explode('_', $fileName);
        $weekNumber = $fileParts[3];
        $year = $fileParts[2];

        $csv = array_map('str_getcsv', file($csvFile));
        // TODO sort here to ensure no duplicate data is inserted (have not tested the below worked)
        array_shift($csv);
        usort($csv, "MyBuilder\CodeTestNPS\Command\ImportNPSScoreCommand::checkId");

        $userIdWas = "";
        foreach ($csv as $csvRow) {
            $userId = $csvRow[0];
            if ($userId !== $userIdWas) {
                $userIdWas = $userId;
                $score = $csvRow[1];

                $sql = "INSERT INTO np_score(user_id, score, week_number, year) VALUES(:user_id, :score, :week_number, :year)";
                $statement = $db->prepare($sql);
                $statement->execute([
                    ':user_id' => $userId,
                    ':score' => $score,
                    ':week_number' => $weekNumber,
                    ':year' => $year
                ]);
            }
        }
        $output->writeln(count($csv) . " records imported.");
    }
}
