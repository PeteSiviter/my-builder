<?php

namespace MyBuilder\CodeTestNPS\Command;

use Exception;
use PDO;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OutputNPSScoreCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('mybuilder:code-test:output-nps-scores')
            ->setDescription('This command outputs NPS scores from the give db')
            ->addArgument('dbpath', InputArgument::REQUIRED, 'Path to the database')
            ->addArgument('week', InputArgument::REQUIRED, 'Week Number')
            ->addArgument('year', InputArgument::OPTIONAL, 'Year number (current if omitted)', date("Y"));
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dbFile = $input->getArgument('dbpath');
        $week = $input->getArgument('week');
        $year = $input->getArgument('year');

        if (empty($dbFile) || (!file_exists($dbFile))) {
            throw(new Exception("Incomplete db file: $dbFile"));
        }

        if (!((1 <= $week) && ($week <= 53))) {
            throw(new Exception("Invalid week number: $week"));
        }

        $output->writeln("Presenting (drumroll)... NPS Score for week number: " . $week . "!");

        $db = new PDO("sqlite:$dbFile");

        // Promoters (score 9-10)
        // Passives (score 7-8)
        // Detractors (score 0-6)
        $sql = 'select count(*) from np_score where np_score.week_number = :week and np_score.year = :year';
        $statement = $db->prepare($sql);
        $statement->execute([
            ':week' => $week,
            ':year' => $year
        ]);
        $total = (int)$statement->fetch()[0];

        $sql = 'select count(*) from np_score where np_score.week_number = :week and np_score.year = :year and np_score.score >= 9 and np_score.score <= 10';
        $statement = $db->prepare($sql);
        $statement->execute([
            ':week' => $week,
            ':year' => $year
        ]);
        $promoters = (int)$statement->fetch()[0];

        $sql = 'select count(*) from np_score where np_score.week_number = :week and np_score.year = :year and np_score.score >= 0 and np_score.score <= 6';
        $statement = $db->prepare($sql);
        $statement->execute([
            ':week' => $week,
            ':year' => $year
        ]);
        $detractors = (int)$statement->fetch()[0];

        // The NP score is calculated by taking the percentage of customers who are Promoters and
        // subtracting the percentage who are Detractors. Note divide by zero protection.
        $npsScore = ($total == 0) ? 0 : (int)(100 * ($promoters - $detractors) / $total);

        $output->writeln("NPS Score is: $npsScore");
    }
}
