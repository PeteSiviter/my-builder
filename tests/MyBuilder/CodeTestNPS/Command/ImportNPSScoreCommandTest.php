<?php

namespace MyBuilder\CodeTestNPS\Command;

use PHPUnit\Framework\TestCase;

class ImportNPSScoreCommandTest extends TestCase
{
    protected ImportNPSScoreCommand $importStub;
    /**
     * @return void
     */
    public function setUp(): void
    {
        // Create a stub for the ImportNPSScoreCommand class.
        $this->importStub = $this->createMock(ImportNPSScoreCommand::class);

        // Configure the stub.
        $this->importStub->method('getProjectDir')
            ->willReturn('/my_pretend_directory');
    }


    /**
     * randomly testing this method for the test, it will always pass as the method in the SUT is mocked.
     * @return void
     */
    public function testGetProjectDir()
    {
        $this->assertTrue(true);
        $this->assertEquals('/my_pretend_directory', $this->importStub->getProjectDir());
    }
}
